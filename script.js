//Перше завдання
const person = {
    name: 'John',
    age: 30,
    gender: 'male'
}

let newPerson = {...person}
newPerson.age = 35

console.log(person); // { name: 'John', age: 30, gender: 'male' }
console.log(newPerson); // { name: 'John', age: 35, gender: 'male' }


//Друге завдання
const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
};
console.log(car)
function printCarInfo(car) {
    if (car.year <= 2001) {
        console.log("Машина занадто стара")
    }
}
printCarInfo(car); // Make: Toyota, Model: Corolla, Year: 2015

//Третє завдання
let str3 = "cdspfdbcjsp dhsuiapfdhysu fdsuafhdsuafudsa usadusadhusa dsuhuiadhsu dsa"
console.log(str3)
function countWords(str) {
    return str.split(' ').length
}
console.log(countWords(str3))

//Четверте завдання
const str4 = 'JavaScript is awesome!';

function reverseString(str4) {
    let splitStr = str4.split("")
    let a = splitStr.reverse()
    let join = a.join("")
    return join
}
console.log(reverseString(str4)); // '!emosewa si tpircSavaJ'


//П'яте завдання
const str5 = 'JavaScript is awesome!';

function reverseWordsInString(str) {
    let splitStr2 = str5.split("")
    // console.log(splitStr2)
    let b = splitStr2.reverse()
    // console.log(b)
    let join2 = b.join("")
    // console.log(join2)
    let splitStr3 = join2.split(" ")
    let b2 = splitStr3.reverse()
    let join3 = b2.join(" ")
    return join3
}

console.log(reverseWordsInString(str5)); // 'tpircSavaJ si !emosewa'